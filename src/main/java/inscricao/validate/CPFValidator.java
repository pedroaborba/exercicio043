package inscricao.validate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("cpfValidator")
public class CPFValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        String value = o.toString();
        
        int numberChars = value.length();
        String pattern = "";        
        switch(numberChars) {
            case 11:
                pattern = "(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d)(\\d\\d)";
                break;
            case 12:
                pattern = "(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d)\\-(\\d\\d)";
                break;
            case 14:
                pattern = "(\\d\\d\\d)\\.(\\d\\d\\d)\\.(\\d\\d\\d)\\-(\\d\\d)";
                break;
            default:
                FacesMessage msg = new FacesMessage("CPF inválido");
                throw new ValidatorException(msg);
        }
        
        Pattern re = Pattern.compile(pattern);
        Matcher matcher = re.matcher(value);
        
        if(!matcher.find()) {
            FacesMessage msg = new FacesMessage("CPF inválido");
            throw new ValidatorException(msg);  
        }
    }
    
}

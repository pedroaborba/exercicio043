package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import inscricao.entity.Idioma;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import utfpr.faces.support.PageBean;

@Named
@RequestScoped
public class InscricaoBean extends PageBean {
    private static final Idioma[] idiomas = {
        new Idioma(1, "Inglês"),
        new Idioma(2, "Alemão"),
        new Idioma(3, "Francês")
    };
    private Candidato candidato = new Candidato(idiomas[0]); // inicialmente ingles
    private List<SelectItem> idiomaItemList;
    
    private ListDataModel dataTableModel = null;
    @Inject
    RegistroBean bean;
    
    public InscricaoBean() {
    }

    public Candidato getCandidato() {
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    public List<SelectItem> getIdiomaItemList() {
        if (idiomaItemList != null) return idiomaItemList;
        idiomaItemList = new ArrayList<>();
        for (Idioma id: idiomas) {
            idiomaItemList.add(new SelectItem(id.getCodigo(), id.getDescricao()));
        }
        return idiomaItemList;
    }

    public String confirmaAction() {
        candidato.setDataHora(new Date());
        candidato.setIdioma(idiomas[candidato.getIdioma().getCodigo()-1]);
        //bean = (RegistroBean) getBean("registroBean");
        bean.getRegistro().add(candidato);
        return "confirma";
    }
    
    public ListDataModel getRegistroData() {
        if(dataTableModel==null) {
            dataTableModel = new ListDataModel(bean.getRegistro());
        }
        
        return dataTableModel;
    }
    
    public void candidatoAction(Object o) {
        this.candidato = (Candidato)o;
        try {
            FacesContext.getCurrentInstance().getExternalContext().dispatch("/inscricao.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(InscricaoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void excluirAction(Object o) {
        bean.getRegistro().remove((Candidato)o);
        try {
            FacesContext.getCurrentInstance().getExternalContext().dispatch("/candidatos.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(InscricaoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

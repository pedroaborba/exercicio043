package inscricao.convert;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("cpfConverter")
public class CPFConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        String pattern = ""; 
        int numberChars = string.length();
        switch(numberChars) {
            case 11:
                pattern = string;
                break;
            case 12:
                pattern = string.substring(0,9)
                        + string.substring(10,12);
                break;
            case 14:
                pattern = string.substring(0,3)
                        + string.substring(4,7)
                        + string.substring(8,11)
                        + string.substring(12,14);
                break;
            default:
                FacesMessage msg = new FacesMessage("CPF inválido");
                throw new ConverterException(msg);
        }
        
        long cpfLong = 0;
        
        try {
            cpfLong = Long.parseLong(pattern);
        } catch(NumberFormatException e) {
            FacesMessage msg = new FacesMessage("CPF inválido");
            throw new ConverterException(msg);
        }
        return cpfLong;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        StringBuilder cpf = new StringBuilder();
        cpf.append(String.valueOf((long)o));
        if(cpf.length()==10) {
            cpf.insert(0,"0");
        }
        cpf.insert(9,"-");
        cpf.insert(6,".");
        cpf.insert(3,".");
        
        return cpf.toString();
    }
    
}
